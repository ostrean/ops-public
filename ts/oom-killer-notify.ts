#!deno --allow-run

main();

async function main() {
    // result: " 4 21:12"
    const datePattern = await sh("date -d '1 minute ago' +'%_d %H:%M'");
    const lines = await sh(`cat /var/log/messages | grep "${datePattern}" | grep -i 'killed process'`);
    if (lines.length > 0) {
        sh(`curl -X POST -H 'Content-type: application/json' --data '{"text":"${lines}"}' https://hooks.slack.com/services/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`);
    }
}

async function sh(cmd) {
    console.log(`execute: ${cmd}`);

    const p = Deno.run({args: ["bash", "-c", cmd,], stdout: "piped", stderr: "piped"});

    const {code} = await p.status();

    if (code === 0) {
        const rawOutput = await p.output();
        return new TextDecoder().decode(rawOutput).trim();
    } else {
        const rawError = await p.stderrOutput();
        const errorString = new TextDecoder().decode(rawError);
        console.log(`ERROR: ${errorString}`);
        return "";
    }
}

