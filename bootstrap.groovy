#!/usr/bin/env groovy

node {
    def scmVars = checkout scm

    sh 'ls -la'

    sh 'rm -rf ops-pipelines'
    sh 'rm -rf ops-pipelines@tmp'

    dir('ops-pipelines') {
        checkout([$class                           : 'GitSCM',
                  branches                         : [[name: '*/master']],
                  doGenerateSubmoduleConfigurations: false,
                  extensions                       : [],
                  submoduleCfg                     : [],
                  userRemoteConfigs                : [[credentialsId: 'bitbucket-aav-login',
                                                       url          : 'https://bitbucket.org/ostrean/ops-pipelines.git']]
        ])
    }

    //git@bitbucket.org:ostrean/ostrean-identity.git
    def gitUrl = sh(returnStdout: true, script: 'git config remote.origin.url').trim()

    String projectFile = "ops-pipelines/" + gitUrl
            .split("bitbucket.org[:/]")[1]
            .replaceAll("\\.git", "\\.groovy") as String


    env.PROJECT_NAME = gitUrl
            .split("bitbucket.org[:/]")[1]
            .split("/")[1]
            .replaceAll("\\.git", "")

    env.GIT_LAST_COMMIT_MESSAGE = sh(script: "git log -n 1 --pretty=%B ${scmVars.GIT_COMMIT} || echo 'no commit message found, PR?'", returnStdout: true).trim()

    println "Checking for the specific project pipeline: $projectFile"

    if (fileExists(projectFile)) {
        println "Load $projectFile"
        load projectFile
    } else {
        println "No specific file found. Load generic 'ops-pipelines/generic.groovy'"
        load "ops-pipelines/generic.groovy"
    }
}

