def call() {
    def version = "unk_version"
    try {
        version = sh(script: "/var/jenkins_home/ops-version print", returnStdout: true).trim()
        println("===>>>>  version == $version")
    } catch (Exception e) {
        System.err.println("can't get version for project")
        return "unk_version"
    }
    return version
}
