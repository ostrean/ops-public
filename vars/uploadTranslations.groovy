def call(String projectName = "", String locumIpPort = "10.44.12.51:9012") {

    sh 'find src -name "messages*.properties"'

    String calculatedName = sh(returnStdout: true, script: 'git config remote.origin.url')
            .trim()
            .split("bitbucket.org[:/]")[1]
            .replaceAll("\\.git", "")
            .replaceAll("ostrean/", "") as String

    if (projectName == null || projectName == "") {
        projectName = calculatedName
    }

    timeout(time: 30, unit: 'SECONDS') {
        sh "find src -name \"messages*.properties\" -exec curl -H 'Content-Type: multipart/form-data' -F 'workspace=microservices' -F 'project=$projectName' -F 'files=@{}' http://$locumIpPort/api/Files \\;"
    }
}
