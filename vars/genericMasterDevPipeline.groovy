def call(Map map) {

    if (map.devDockerTag == null) map["devDockerTag"] = "latest"

    def scmVars = checkout scm
    setBuildNumber(scmVars)

    env.GIT_LAST_COMMIT_MESSAGE = sh(script: "git log -n 1 --pretty=%B ${scmVars.GIT_COMMIT} || echo 'no commit message found, PR?'", returnStdout: true).trim()
    print("last commit message: " + env.GIT_LAST_COMMIT_MESSAGE)

    def version = getCurrentVersion()

    if (env.BRANCH_NAME == "Develop" || env.BRANCH_NAME == "dev") {

        lock("${map.serviceShortName}/Develop") {

            stage('upload translations') {
                uploadTranslations("", "10.44.12.202:9012")
            }

            buildAndDeployToDevelopStage(map)

        }

    } else if (env.BRANCH_NAME == "master") {

        if (env.GIT_LAST_COMMIT_MESSAGE == null) {
            env.GIT_LAST_COMMIT_MESSAGE = ""
        }

        //same variable have to be changed in ops-pipelines/ostrean/job_seed/bump_version.groovy
        def NEW_RELEASE_COMMIT_MESSAGE = "[new release version"

        if (env.GIT_LAST_COMMIT_MESSAGE.startsWith(NEW_RELEASE_COMMIT_MESSAGE)) {

            stage('new release commit') {

            }

            stage('upload translations') {
                uploadTranslations("", "10.44.12.102:9012")
            }

            buildAndDeployToPreProdStage(map, version)
        } else {

            stage('new release commit') {
                build job: "/create-new-release-version/${env.PROJECT_NAME}"
            }

        }

    } else {
        //ALL OTHER BRANCHES

        def BUILD_USER_ID = null;
        wrap([$class: 'BuildUser']) {
            BUILD_USER_ID = env.BUILD_USER_ID
        }
        println("BUILD_USER_ID --> " + BUILD_USER_ID)

        if (BUILD_USER_ID == null) { //MEANS THAT IT WAS TRIGGERED BY WEBHOOK
            stage('build containers') {
                timeout(15) {
                    sh "docker build --build-arg VERSION=0.1.0 -t ostrean${map.serviceShortName}:test -f src/Dockerfile ."
                }
            }
        } else {
            buildAndDeployToDevelopStage(map)
            // buildAndDeployToPreProdStage(map, version)
        }
    }
}

def buildAndDeployToDevelopStage(Map map) {
    stage('build + tests') {
        timeout(15) {
            sh "docker build --build-arg VERSION=0.1.0 --target build -t ostrean${map.serviceShortName}:${map.devDockerTag} -f src/Dockerfile ."
            }
    }

    stage('build container') {
        timeout(15) {
            sh "docker build --build-arg VERSION=0.1.0 -t docker-int.devostrean.com/ostrean${map.serviceShortName}:${map.devDockerTag} -f src/Dockerfile ."

            retry(5) {
                sh "docker push docker-int.devostrean.com/ostrean${map.serviceShortName}:${map.devDockerTag}"
                sleep(5)
            }

        }
    }

    stage('restart containers') {
        build job: "/ansible-scope-dev/ms-${map.serviceShortName}.yml"
    }

    stage('logs DEV') {
        sh "ssh root@10.44.12.202 journalctl -u ${map.serviceShortName}_container.service --since \\\"1 minute ago\\\""
    }

    stage('health check') {
        healthCheck(map.devHealthCheckUrl)
    }
}

def buildAndDeployToPreProdStage(Map map, String version) {
    stage('build + tests') {
        timeout(15) {
            sh "docker build --build-arg VERSION=${version} --target build -t ostrean${map.serviceShortName}:${map.devDockerTag} -f src/Dockerfile ."
        }
    }

    stage('build container') {
        timeout(15) {
            sh "docker build --build-arg VERSION=${version} -t docker-int.devostrean.com/ostrean${map.serviceShortName}:${version} -f src/Dockerfile ."
            retry(5) {
                sh "docker push docker-int.devostrean.com/ostrean${map.serviceShortName}:${version}"
                sleep(5)
            }
        }
    }

    stage('restart containers') {
        build job: "/ansible-scope-staging/ms-${map.serviceShortName}.yml", parameters: [[$class: 'StringParameterValue', name: 'PARAM_DOCKER_TAG', value: version]]
    }

    stage('logs STAGE') {
        sh "ssh root@10.44.12.102 journalctl -u ${map.serviceShortName}_container.service --since \\\"1 minute ago\\\""
    }

    stage('health check') {
        healthCheck(map.masterHealthCheckUrl)
    }

    stage('log deployed version') {
        writeDeploymentLog([scope: "staging", version: version, repoName: env.PROJECT_NAME])
    }
}