def call(Map map) {

    println("writeDeploymentLog args: " + map)

    def baseFolder = '/var/jenkins_home/custom_resource/ops-cloud-state'

    lock('writeDeploymentLog') {

        if (!new File(baseFolder).exists()) {
            dir("/var/jenkins_home/custom_resource/") {
                sh "git clone git@bitbucket.org:ostrean/ops-cloud-state.git"
            }
        }

        dir("$baseFolder") {
            sh "pwd"
            sh "git fetch"
            sh "git reset --hard origin/master"

            def stageOrProdFolder = "unk-scope"
            if (map.scope == "prod") {
                stageOrProdFolder = "prod"
            } else if (map.scope == "staging") {
                stageOrProdFolder = "staging"
            }

            new File("$baseFolder/$stageOrProdFolder/").mkdirs()
            def file = new File("$baseFolder/$stageOrProdFolder/${map.repoName}.txt")
            if (!file.exists()) {
                file.createNewFile()
            }
            file.text = "${map.version}"

            sh "git add ."
            sh "git commit -m \"scope $stageOrProdFolder, project ${map.repoName}, version ${map.version} deployed \" --allow-empty"
            sh "git push"
        }
    }
}
