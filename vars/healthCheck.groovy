def call(urlToCheck) {

    if (urlToCheck.contains(":6007/hc")) {
        println("skip HC for ostrean-encryptionagent")
        return "OK"
    }

    timeout(time: 90, unit: 'SECONDS') {
        script {
            def maxRetries = 60
            def retry = 0            
            while(retry < maxRetries)
            {
                retry++
                try {
                    final String response = sh(script: "curl -s $urlToCheck", returnStdout: true).trim()
                    //echo "Healthcheck response: $response"
                    
                    def json = new groovy.json.JsonSlurperClassic().parseText(response)
                    def status = json.status
                    echo "Health check status: $status"
                    
                    if (status == 'Healthy' || status == 'Unhealthy')
                        break
                                      
                } catch (Exception e) {
                   echo "Failed to get health check status, retrying in 1 second..."
                   echo 'Exception: ' + e.toString()
                }
                
                sleep(1)
            }
            
            if (retry == maxRetries) {
                error("Failed to get health status after ${maxRetries} retries.")
            }
        }
    }
}
